import numpy as np

def get_minima(results, threshold):

    ascending = True
    minimums = []
    last_frame, last_value = results[0]
    for i in range(1, len(results)):
        frame, value = results[i]
        if last_value < value:
            # insert minimums
            if not ascending:
                minimums.append((last_frame, last_value))
            ascending = True
        else:
            ascending = False
        last_frame = frame
        last_value = value
    minimums = [(frame, value) for frame, value in minimums\
                               if value > threshold]
    return np.array(minimums)
