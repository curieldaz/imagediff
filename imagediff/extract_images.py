import os

import cv2

def extract_images(frames,
                   video_path,
                   output_folder="../outputs/"):

    filename = video_path.split("/")[-1]
    video_folder = filename.split(".")[0] + "/"
    if not os.path.exists(output_folder + video_folder):
        os.makedirs(output_folder + video_folder)

    cap = cv2.VideoCapture(video_path)
    if not cap.isOpened():
        print("Error opening video stream or file")

    width = cap.get(cv2.CAP_PROP_FRAME_WIDTH)
    height = cap.get(cv2.CAP_PROP_FRAME_HEIGHT)

    for frame_no in frames:
        cap.set(cv2.CAP_PROP_POS_FRAMES, frame_no)
        ret, frame = cap.read()
        cv2.imwrite(output_folder + video_folder + "%2d.png" % (frame_no), frame)
    cap.release()
