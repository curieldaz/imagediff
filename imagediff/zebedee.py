import numpy as np
import matplotlib.pyplot as plt

plt.rcParams.update({'font.size': 22})

import re
import os

input_dir = "../zebedescriptions/ZBD-v2"

# Get key_posture distribution
distribution = {}
total_signs = 0
for root, _, files in os.walk(input_dir):
    for filename in files:
        if filename.endswith(".zbd"):
            filepath = os.path.join(root, filename)
            contents = ""
            with open(filepath, "r") as fp:
                contents = fp.read()
            if "SEQUENCE" in contents:
                matches = re.findall("SEQUENCE\s*\"(.+?)\"(.+?)End", contents.replace("\n", ""))
                for sign, match in matches:
                    total_signs += 1
                    total_key_postures = match.count("KEY_POSTURE")
                    distribution[total_key_postures] = distribution.get(
                        total_key_postures, [])
                    distribution[total_key_postures].append(sign)

# do graphs
counts = []
for i in range(max(distribution.keys()) + 1):
    try:
        total = len(distribution[i])
    except:
        total = 0
    counts.append(total)

y_axis = np.arange(len(counts))
plt.bar(y_axis, counts)
plt.show()
