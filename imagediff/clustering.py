from sklearn.cluster import AgglomerativeClustering
from scipy.cluster.hierarchy import dendrogram, linkage
from skimage.measure import compare_ssim

import matplotlib
matplotlib.use("Agg")
import matplotlib.pyplot as plt

import cv2
import os
import sys
import numpy as np

if len(sys.argv)!=2:
    raise Exception()
elif not os.path.exists(sys.argv[1]):
    raise Exception()

result = []
dataset = None
for filename in os.listdir(sys.argv[1]):
    img = cv2.imread(sys.argv[1] + filename)
    grayimg = cv2.cvtColor(cv2.medianBlur(img, 5), cv2.COLOR_BGR2GRAY)
    result.append(int(filename.split(".")[0]))
    try:
        dataset = np.vstack([dataset, grayimg.flatten()])
        #dataset = np.vstack([dataset, grayimg])
    except Exception as e:
        print(e)
        dataset = grayimg.flatten()
        #dataset = grayimg

results = np.array(result)
#cluster = AgglomerativeClustering(n_clusters=2, affinity='euclidean', linkage='ward')
#cluster.fit_predict(dataset)
fig = plt.figure(figsize=(11.69, 8.27))

#z = linkage(dataset, 'average', metric=compare_ssim)
z = linkage(dataset, 'ward')
dn = dendrogram(z, labels=result)

plt.savefig("../dendrogram_%s.png" % sys.argv[1].split("/")[-1])
fig.clf()
#plt.show()

