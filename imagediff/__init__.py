# import the necessary packages
from minima import get_minima
import matplotlib.pyplot as plt
import numpy as np
from skimage.metrics import structural_similarity as ssim
import argparse
import cv2

from imagediff.extract_images import extract_images

def main():
    # construct the argument parse and parse the arguments
    ap = argparse.ArgumentParser()
    ap.add_argument("-v", "--video", required=True,
    	help="input video")
    args = vars(ap.parse_args())
    
    cap = cv2.VideoCapture(args["video"])
    
    if cap.isOpened() == False:
        print("Error opening video stream or file")
    
    imageA = None
    first = None
    results = []
    frame = -1
    while cap.isOpened() :
        ret, imageB = cap.read()
        if ret == True:
            frame += 1
            if first == None:
                imageA = imageB.copy()
                first = False
                continue
    
            # convert the images to grayscale
            grayA = cv2.cvtColor(cv2.medianBlur(imageA, 5), cv2.COLOR_BGR2GRAY)
            grayB = cv2.cvtColor(cv2.medianBlur(imageB, 5), cv2.COLOR_BGR2GRAY)
    
            # compute the Structural Similarity Index (SSIM) between the two
            # images, ensuring that the difference image is returned
            (score, diff) = ssim(grayA, grayB, gaussian_weights=True, full=True)
            diff = (diff * 255).astype("uint8")
            score = float("%.2f" % (score))
            print("SSIM: {} ({})".format(1 - score, frame))
            results.append(1 - score)
    
            # threshold the difference image, followed by finding contours to
            # obtain the regions of the two input images that differ
            thresh = cv2.threshold(diff, 0, 255,
            cv2.THRESH_BINARY_INV | cv2.THRESH_OTSU)[1]
            #cnts = cv2.findContours(thresh.copy(), cv2.RETR_EXTERNAL,
            #cv2.CHAIN_APPROX_SIMPLE)
            #cnts = imutils.grab_contours(cnts)
    
            ## loop over the contours
            #for c in cnts:
            #    # compute the bounding box of the contour and then draw the
            #    # bounding box on both input images to represent where the two
            #    # images differ
            #    (x, y, w, h) = cv2.boundingRect(c)
            #    cv2.rectangle(imageA, (x, y), (x + w, y + h), (0, 0, 255), 2)
            #    cv2.rectangle(imageB, (x, y), (x + w, y + h), (0, 0, 255), 2)
    
            #show the output images
            #cv2.imshow("%d" % frame, imageA)
            #cv2.imshow("Modified", imageB)
            #cv2.imshow("Diff", diff)
            #cv2.imshow("Thresh", thresh)
            #cv2.waitKey(0)
            imageA = imageB
        else:
            cap.release()
    results = [(i, value) for i, value in enumerate(results)]
    minima = get_minima(results, 0.01)
    
    # extract images to folder
    extract_images(minima[:, 0], args['video'])
    
    results = np.array(results)
    plt.plot(results[:, 0],
             results[:, 1])
    plt.plot(minima[:, 0],
             minima[:, 1],
             "o")
    plt.show()
    plt.close()

if __name__ == "__main__":
    main()
