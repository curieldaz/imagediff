import os

import cv2

import numpy as np
import pandas as pd

from skimage.metrics import structural_similarity as ssim
from skimage.metrics import mean_squared_error as mse
from skimage.util import img_as_float

from imagediff.util import show_img 
from imagediff.minima import get_minima

def main(data="./data", minima_tolerance=.01):
    sl_records = []
    for root, _, fnames in os.walk(data):
        
        if not len(fnames) > 0:
            continue

        sl_name = os.path.basename(root)
        sign_id = 1
        for fname in fnames:
            sign_record = {} 
            full_path = os.path.join(root, 
                                     fname)
            # Analyse videos
            capture = cv2.VideoCapture(full_path)
            if capture.isOpened() == False:
                print(f"Couldn't open {full_path}")
                continue
            # Loop frames
            i=0
            last_frame = None
            ssim_values = []
            frame_memory = {}
            while capture.isOpened():
                ret, frame = capture.read()
                if ret == True:
                    gray_frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
                    gray_frame = cv2.medianBlur(gray_frame, 5)
                    frame_memory[i] = gray_frame

                    if i == 0:
                        ssim_values.append((i,
                                            1 - ssim(
                                                  gray_frame, 
                                                  np.zeros(gray_frame.shape,
                                                           dtype="uint8"),
                                                  gaussian_weights=True)))
                    else:
                        ssim_values.append((i,
                                            1 - ssim(gray_frame, 
                                                     last_frame, 
                                                     gaussian_weights=True)))
                    last_frame = gray_frame
                    i += 1
                    #show_img(frame, f"frame {i}")
                else:
                    break
            capture.release()
            min_vals = get_minima(ssim_values, minima_tolerance)

            try:
                last_i, last_value = min_vals[0]
            except:
                continue
            min_differences = []
            for k in range(1, min_vals.shape[0]):
                i, value = min_vals[k]
                # Check difference between minimums
                first_min_img = img_as_float(frame_memory[int(last_i)])
                second_min_img = img_as_float(frame_memory[int(i)])
                diff = mse(first_min_img, second_min_img)
                min_differences.append(diff)
                last_i = i
                last_value = value

            # TODO: Duration

            # Record
            sign_record["sign_id"] = sign_id
            sign_record["sl_name"] = sl_name
            sign_record["min_segments"] = min_vals.shape[0]
            sign_record["avg_difference"] = np.mean(min_differences)
            for i in range(len(min_differences)):
                sign_record[f"diff_{i}"] = min_differences[i]

            sl_records.append(sign_record)
            sign_id += 1
            print(f"Processed {fname} ({sl_name})...")
    df = pd.DataFrame(sl_records)
    df.to_csv("first_analysis.csv")



if __name__ == "__main__":
    main()
