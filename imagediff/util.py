import cv2

def show_img(img, win_title=None):
    if not win_title:
        win_title = "%dx%d" % img.shape[:2]

    assert isinstance(win_title, str)

    img = img.astype('uint8')
    cv2.namedWindow(win_title, cv2.WINDOW_NORMAL)
    cv2.imshow(win_title, img)
    cv2.resizeWindow(win_title, 1024, 768)

    while cv2.getWindowProperty(win_title,
                                cv2.WND_PROP_VISIBLE) >= 1:
        if cv2.waitKeyEx(1000) == 27:
            cv2.destroyWindow(win_title)
            break

